Webform Remote Select

### About this module

- This module provides a Webform Select Element whose options are populated
from an endpoint through REST services.

### Goals

- Populate select element options with data retrieved with REST services;

### How to use this module

In order to use this module, you should follow these steps:

1. Enable Webform Remote Select module;
2. Create a new Webform (or edit an existing one);
3. Add new Remote Select element:

- Endpoint: Endpoint to get data to populate remote select element;
- Headers: Optional field to use additional headers in request;
- Response data key: Use specific key in response to get items;
- Response items key: Response parameter used as key when populating select 
options;
- Response items value: Response parameter used as label when populating select
options;
- Allow duplicated values: Option to allow users to insert duplicated values in
multiple elements;
- Use empty option: Configuration to show/hide empty option;
- Empty option label: Allows to override default empty option labels
('-Select-' or '-None-');
- Use select2: Allows to use select2 in this Remote Select Element;
