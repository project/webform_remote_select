<?php

namespace Drupal\Tests\webform_remote_select\Functional;

use Drupal\Tests\webform\Functional\WebformBrowserTestBase;

/**
 * Tests for webform example composite.
 *
 * @group webform_example_composite
 */
class WebformRemoteSelectTest extends WebformBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'webform',
    'webform_remote_select',
    'webform_remote_select_test',
  ];

  /**
   * Tests webform remote select element.
   */
  public function testWebformRemoteSelect() {
    $assert_session = $this->assertSession();

    // Check form element rendering.
    $this->drupalGet('/webform/webform_remote_select_test');

    $assert_session->responseContains('<option value="1">test1</option>');
  }

}
