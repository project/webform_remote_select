<?php

namespace Drupal\webform_remote_select_test\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Return sample JSON data used for testing.
 */
class ExampleDataController {

  /**
   * Return data used for testing purposes.
   * 
   * @return JsonResponse
   */
  public function test() {
    return new JsonResponse([['id' => 1, 'title' => 'test1'], ['id' => 2, 'title' => 'test2']]);
  }

}
